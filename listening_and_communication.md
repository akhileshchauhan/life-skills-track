# Listening and Active Communication

### What are the steps/strategies to do Active Listening?
- Use verbal and nonverbal cues to indicate that you are paying attention.
- Avoid getting distracted by our own thoughts.
- Allow the speaker to express themselves fully without rushing or interrupting.
- Take notes during important conversations if necessary.
- If something is unclear or you need more information, ask open-ended questions to gain a deeper understanding.
- Show empathy by acknowledging their feelings and validating their experiences.

### According to Fisher's model, what are the key points of Reflective Listening?
- **Understanding and Empathy**: Aim to understand the speaker's perspective and feelings.
- **Validation:** Validation helps create a supportive and nonjudgmental environment.
- **Clarification:** If there are any points of confusion, the listener seeks clarification from the speaker to ensure mutual understanding.
- **Respect:** a respectful and nonjudgmental attitude toward the speaker.

### What are the obstacles in your listening process?

- **Distractions:** External distractions such as noise, interruptions, or visual stimuli.
- **Poor Listening Skills:** Inadequate listening skills, such as a lack of attention, inability to paraphrase or summarize.
- **Physical Barriers:** Physical barriers such as distance, seating arrangements, or environmental factors.
- **Emotional State:** It's important to be mindful of your emotions and how they may impact your listening process.

### What can you do to improve your listening?

- **Quiet Place**: Find a quiet spot where there aren't many noises to distract you.
- **Look and Listen**: Pay attention to the person speaking and look at them when they talk.
- **Ask Questions**: If you don't understand something then ask questions to learn more.
- **Practice**: Try listening more when people talk, and it will get easier over time. 

### When do you switch to Passive communication style in your day to day life?
  
- **Expressing Anger**: we can switch to aggressive communication when we express anger
- **Dominating Conversations**: when you want to dominate the current conversation.
- **Ignoring Feelings**: You might dismiss or invalidate others' feelings or opinions.

### When do you switch into Passive Aggressive communication styles in your day to day life?
- Passive-aggressive behavior. 
- while using sarcasm
- individuals feel powerless

### How can you make your communication assertive?
- Listen attentively to the other person's perspective and validate their feelings and experiences.
- Clearly express your thoughts, feelings, and needs without being aggressive or passive.
- Make eye contact with the person you're speaking to, as it conveys confidence and shows that you're engaged in the conversation.
- Approach conflicts or disagreements with a collaborative mindset,
seeking mutually beneficial solutions that address the needs and concerns of all parties involved.

