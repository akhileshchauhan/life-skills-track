# Grit and Growth Mindset

### 1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
The importance of grit and a growth mindset not giving up, even when things are really hard. In that situation, we need to keep patience and self-belief on ourselves until the difficulties do not pass.

### 2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

A growth mindset means thinking you can improve if you keep trying and don't give up. It's about believing in yourself and learning from mistakes.

### 3. What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control means feeling like you can control your life. The key point likely revolves around how having an internal locus of control fosters resilience, self-reliance, and a proactive approach to challenges, ultimately leading to greater success and fulfilment.

### 4. What are the key points mentioned by a speaker to build a growth mindset (explanation not needed)?

- Stay strong at your bad face.
- Believe in the power of improvement.
- Learn from criticism.
- Learn from your own mistakes.
- Be inspired by other's success.

### 5. What are your ideas to take action and build a Growth Mindset?

- I am 100 percent responsible for my learning.
- I will understand each concept properly.
- Change "I can't do" to "I'll try."