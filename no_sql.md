# Investigation of five NoSQL Databases and their use cases

## 1. MongoDB:

- MongoDB is a Document-Oriented NoSQL Database that stores data in   JSON-like documents with dynamic schemas, making it highly flexible.

- It's commonly used for applications with large volumes of unstructured or semi-structured data, such as content management systems, real-time analytics, and mobile apps.

- MongoDB provides high availability and scalability through its support for sharding, replication, and automatic failover.

## 2. Cassandra:

- Apache Cassandra is a distributed NoSQL database designed for handling large volumes of data across many commodity servers while providing high availability and fault tolerance.

- It's suitable for applications that require high write throughput and linear scalability, such as IoT data management, time-series data, and messaging platforms.

- Cassandra's decentralized architecture and eventual consistency model make it well-suited for use cases where low-latency reads and writes are essential.

## 3. Redis:

- Redis is an in-memory data structure store often used as a NoSQL database, cache, and message broker.

- It excels in scenarios that require high performance and low latency, such as real-time analytics, session caching, and leaderboards.

- Redis supports various data structures like strings, hashes, lists, sets, and sorted sets, making it versatile for different use cases.

## 4. Amazon DynamoDB:

- DynamoDB is a fully managed NoSQL database service provided by Amazon Web Services (AWS), designed for low-latency, scalable, and high-performance applications.

- It's suitable for use cases like gaming, ad tech, and IoT, where predictable performance and seamless scalability are critical.

- DynamoDB offers features such as automatic scaling, multi-region replication, and built-in security controls, making it easy to use and manage.

## 5. Couchbase:

- Couchbase is a distributed NoSQL database that combines the flexibility of JSON documents with the speed of key-value stores.

- It's used in applications requiring high performance, such as e-commerce, user profiles, and personalization engines.

- Couchbase provides features like data replication, auto-sharding, and in-memory caching to deliver low-latency responses and high availability.

### Reference
* [MongoDB](https://www.mongodb.com/nosql-explained)
* [GeeksForGeeks](https://www.geeksforgeeks.org/open-source-nosql-databases/)