# Prevention of Sexual Harassment  

Prevention of sexual harassment is essential in creating safe and respectful environments, whether in the workplace, educational institutions, or public spaces.Promote a culture of respect, equality, and dignity in all aspects of organizational or institutional life.Regularly review and evaluate policies, procedures, and prevention efforts to identify areas for improvement and ensure effectiveness in preventing sexual harassment.

### What kinds of behaviour cause sexual harassment?

Sexual harassment keep a wide range of behaviors that are unwelcome, inappropriate, and create a hostile or intimidating environment.

Some example of sexual harassment are :

- Making unwanted sexual advances, such as unwanted touching, kissing,   or sexual propositions.
- Sending sexually explicit emails, text messages, or other forms of communication that are unsolicited and unwelcome.
- Sharing or displaying pornographic material in the workplace or educational environment.
- Conditioning employment, promotions, or other benefits on the acceptance of unwelcome sexual advances or favors.

### What would you do in case you face or witness any incident or repeated incidents of such behavior?

Here are some points mentioned if I face this thing repeatedly -

- Keep a record of any incidents of sexual harassment, including dates, times, locations, and details of the behavior.
- Report the incident to a supervisor, human resources, or other appropriate authority within your organization or institution. 
- Encourage others who have experienced or witnessed harassment to report the incident. Offer support and reassurance to individuals who may be hesitant to come forward.
- Familiarize yourself with your rights and protections under relevant laws and policies pertaining to sexual harassment. 
