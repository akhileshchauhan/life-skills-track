# Learning Process

### What is the Feynman Technique? 
Feynman Technique explains that we need to break the bigger problem into simpler one and solve them.

Like Divide and Conquer algorithms which break the larger problem into simpler one then solve all the simpler problems after that merge them.

### What was the most interesting story or idea for you?
The most interesting story for me in this video was don't just follow your passions, broaden your passions, and your life will be enriched beyond measure.

### What are active and diffuse modes of thinking?
- **Active mode:** This is the state of concentrated attention on a particular task or concept. When we're in focused mode, our brain is actively engaged in processing information, solving problems, and learning specific details.

- **Diffuse mode:** Diffused thinking involves a more relaxed and unfocused state of mind. Our thoughts are free to wander, and your brain is not narrowly focused on a specific task or goal.It allows for more associative and creative thinking, as your mind is open to making connections between seemingly unrelated ideas.

### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

- **Deconstruct the skill -** Break the skill down into its most basic parts.
- **Learn enough to self-correct -** Learn enough to realize when you're making mistakes.
- **Remove practice barriers -** Turn off your phone, unplug the TV. Put your guitar, piano etc.
- **Practice at least 20 hours -** Commit to 20 hours from the start. You're going to be frustrated at times, so committing beforehand will help you push through the frustration.

### What are some of the actions you can take going forward to improve your learning process?

I will take some actions further to improve my learning process.

- Keep Attention to the given task and work.

- Keep practicing until you are not satisfied with any topic.

- Stay motivated and keep high expectations from yourself.
