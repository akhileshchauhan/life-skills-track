# Good practices for Software Development

### 1.Which point(s) were new to you?

The points that were new to me were :

- We can track our time using app like "Boosted" to improve productivity.
- Some companies use tools like Trello, Jira for documentation.


### 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?

 **Areas where I think I need to improve are as follows:**

- time management
- communicating on Software requirement
- tracking time while working and take efficient breaks.
- Integrate real-time learning capabilities.
- Incorporate feedback mechanisms:

**Ideas to make progress in that particular area are as follows:**

- Utilize time tracking tools such as "Boosted".
- Make notes while discussing requirements with your team.
- Make attention on important topics.